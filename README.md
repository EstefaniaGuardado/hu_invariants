# Hu Invariants #

Moment invariants of Hu have been widely applied to image pattern recognition in a variety of applications due to its invariant features on image translation, scaling and rotation.

In this project estimated the moments invariants of Hu (1,2,3,4,5), this data is used for apply geometric transformations, rotation and scaling.