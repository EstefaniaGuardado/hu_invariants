﻿using System;
using System.IO;
using System.Drawing;

namespace momentoInvariante
{
	class MainClass
	{

		public int[] medidasDeImagen (double[,] imagen)
		{

			int largo = imagen.GetLength (0);
			int ancho = imagen.GetLength (1);
			int[] cantidades = new int[2];

			cantidades [0] = ancho;
			cantidades [1] = largo;

			return cantidades;

		}

		public double[] centroideImagen (int[] medidas, double[,] imagen)
		{

			double coordenadaY = 0;
			double coordenadaX = 0;
			double numeroPixeles = 0;

			for (int contadorFilas = 0; contadorFilas < medidas[0]; ++contadorFilas) {
				for (int contadorColumnas = 0; contadorColumnas < medidas[1]; ++contadorColumnas) {
					if (imagen [contadorColumnas, contadorFilas] == 1) {
						coordenadaY = coordenadaY + contadorColumnas;
						coordenadaX = coordenadaX + contadorFilas;
						++numeroPixeles;
					}
				}
			}

			double x_Centro = Math.Round(coordenadaX / numeroPixeles);
			double y_Centro = Math.Round(coordenadaY / numeroPixeles);

			double[] puntoCentro = new double[2];
			puntoCentro [0] = x_Centro;
			puntoCentro [1] = y_Centro;

			return puntoCentro;


		}

		public double momentoInvariante (double[,] imagen, int[] tamano, double[] puntosCentro, int p, int q)
		{

			double imagenMomento = 0;

			for (int contadorFilas = 0; contadorFilas < tamano [0]; ++contadorFilas) {
				for (int contadorColumnas = 0; contadorColumnas < tamano [1]; ++contadorColumnas) {
					double primerPaso = Math.Pow ((contadorColumnas - puntosCentro [0]), p);
					double segundoPaso = Math.Pow ((contadorFilas - puntosCentro [1]), q);
					double punto = imagen [contadorColumnas, contadorFilas];
					double tercerPaso = primerPaso * segundoPaso * punto;

					imagenMomento = imagenMomento + tercerPaso;
				}
			}



			return imagenMomento;

		}


		public double suma (double M1, double M2)
		{

			double invarianteSuma = M1 + M2;

			return invarianteSuma;

		}

		public double resta (double M1, double M2)
		{

			double invarianteSuma = M1 - M2;

			return invarianteSuma;

		}

		public double potencia (double M1, int potencia)
		{

			double invariantePotencia = Math.Pow (M1, potencia);

			return invariantePotencia;
		}

		public double multiplicacion (double M1, double multiplicador)
		{

			double invarianteMultiplicacion = multiplicador * M1;

			return invarianteMultiplicacion;
		}


		public double invarianteDos (double M20, double M02, double M11)
		{


			double restaInvarianteDos = resta (M20, M02);

			double potenciaInvarianteDos_Parte1 = potencia (restaInvarianteDos, 2);

			double potenciaInvarianteDos_Parte2 = potencia (M11, 2);

			double multiplicacionInvarianteDos = multiplicacion (potenciaInvarianteDos_Parte2, 4);

			double invarianteDosResultado = suma (multiplicacionInvarianteDos, potenciaInvarianteDos_Parte1);


			return invarianteDosResultado;

		}

		public double invarianteTres (double M30, double M12, double M21, double M03)
		{

			double multiplicacionInvarianteTres_Parte1 = multiplicacion (M12, 3);

			double restaInvarianteTres_Parte1 = resta (M30, multiplicacionInvarianteTres_Parte1);

			double potenciaInvarianteTres_Parte1 = potencia (restaInvarianteTres_Parte1, 2);

			double multiplicacionInvarianteTres_Parte2 = multiplicacion (M21, 3);

			double restaInvarianteTres_Parte2 = resta (multiplicacionInvarianteTres_Parte2, M03);

			double potenciaInvarianteTres_Parte2 = potencia (restaInvarianteTres_Parte2, 2);

			double invarianteTresResultado = suma (potenciaInvarianteTres_Parte1, potenciaInvarianteTres_Parte2);


			return invarianteTresResultado;

		}

		public double invarianteCuatro(double M30, double M12, double M03){

			double sumaPrimeraParte = suma (M30, M12);

			double potenciaPrimeraParte = potencia (sumaPrimeraParte, 2);

			double sumaSegundaParte = suma (M12, M03);

			double potenciaSegundaParte = potencia (sumaSegundaParte, 2);

			double invarianteCuatroResultado = suma (potenciaPrimeraParte, potenciaSegundaParte);

			return invarianteCuatroResultado;
		}
			
		public double invarianteCinco(double M30, double M12, double M21, double M03){

			double multiplicacionPrimeraParte = multiplicacion (M12, 3);

			double sumaPrimeraParte = suma (multiplicacionPrimeraParte, M30); //

			double sumaSegundaParte = suma (M30, M12); //

			double sumaTerceraParte = suma (M30, M12);

			double potenciaTerceraParte = potencia (sumaTerceraParte, 2);

			double sumaTerceraParteDos = suma (M21, M03);

			double potenciaTercerParteDos = potencia (sumaTerceraParteDos, 2);

			double multiplicacionTerceraParteDos = multiplicacion (potenciaTercerParteDos, 3);

			double restaTerceraParte = resta (potenciaTerceraParte, multiplicacionTerceraParteDos); //

			double primerMultiplicacion = multiplicacion (sumaPrimeraParte, sumaSegundaParte);

			double primerMultiplicacionResutlado = multiplicacion (primerMultiplicacion, restaTerceraParte); //---

			double multilplicacionCuartaParte = multiplicacion (M21, 3); 

			double sumaCuartaParte = suma(multilplicacionCuartaParte, M03); //

			double sumaQuintaParte = suma (M21, M03); //

			double sumaSextaParte = suma (M30, M12);

			double potenciaSextaParte = potencia (sumaSextaParte, 2);

			double multiplicacionSextaParte = multiplicacion (potenciaSextaParte, 3);

			double sumaSextaParteDos = suma (M21, M03);

			double potenciaSextaParteDos = potencia (sumaSextaParteDos, 2);

			double restaSextaParte = resta (multiplicacionSextaParte, potenciaSextaParteDos); //

			double segundaMultiplicacion = multiplicacion(sumaCuartaParte, sumaQuintaParte);

			double segundaMultiplicacionResultado = multiplicacion (segundaMultiplicacion, restaSextaParte);

			double invarianteCincoResultado = suma (primerMultiplicacionResutlado, segundaMultiplicacionResultado);

			return invarianteCincoResultado;
		}

		private Bitmap rotateImage(float angle)
		{
			string bitmpaFilePath = @"/Users/estefaniachavezguardado/Downloads/prueba1.png";
			Bitmap returnBitmap = new Bitmap (bitmpaFilePath);
			Graphics g = Graphics.FromImage (returnBitmap);

			g.TranslateTransform ((float)returnBitmap.Width/2, (float)returnBitmap.Height/2);
			g.RotateTransform (angle);
			g.TranslateTransform (-(float)returnBitmap.Width/2, -(float)returnBitmap.Height/2);

			g.DrawImage (returnBitmap, new Point (0, 0));

			return returnBitmap;
		}

		private Bitmap scaleImage(float scale)
		{
			string bitmpaFilePath = @"/Users/estefaniachavezguardado/Downloads/prueba1.png";
			Bitmap returnBitmap = new Bitmap (bitmpaFilePath);
			Graphics g = Graphics.FromImage (returnBitmap);

			g.ScaleTransform (scale, scale);

			g.DrawImage (returnBitmap, new Point (0, 0));

			return returnBitmap;

		}

		public Bitmap obtenerImagen(){
			string bitmpaFilePath = @"/Users/estefaniachavezguardado/Downloads/prueba1.png";
			Bitmap imagen = new Bitmap (bitmpaFilePath);

			return imagen;
		}

		public double[,] Binario(Bitmap Bmp)
		{

			int rgb;
			Color c;
			int umbral = 95;
			int Height = Bmp.Height;
			int Width = Bmp.Width;
			double[,] matriz = new double[Width,Height];

			for (int y = 0; y < Bmp.Width; y++)
				for (int x = 0; x < Bmp.Height; x++)
				{
					c = Bmp.GetPixel(y, x);
					rgb = (int)(c.R + c.G + c.B);
					matriz [y, x] = rgb < umbral ? 1 : 0;
				}
			
			return matriz;
		}

		public void principal(Bitmap imagen){

			double[,] picture = Binario (imagen);

			int[] tamano = medidasDeImagen(picture);

			double[] centroide = centroideImagen(tamano, picture);


			int p = 2;
			int q = 0;

			double momentoResultadoDosUno = momentoInvariante(picture, tamano, centroide, p, q);

			p = 0;
			q = 2;

			double momentoResultadoCeroDos = momentoInvariante(picture, tamano, centroide, p, q);

			double InvarianteUno = suma(momentoResultadoDosUno, momentoResultadoCeroDos);


			p = 1;
			q = 1;

			double momentoResultadoUnoUno = momentoInvariante(picture, tamano, centroide, p, q);

			double InvarianteDos = invarianteDos(momentoResultadoDosUno, momentoResultadoCeroDos, momentoResultadoUnoUno);


			p = 3;
			q = 0;

			double momentoResultadoTresCero = momentoInvariante(picture, tamano, centroide, p, q);

			p = 1;
			q = 2;

			double momentoResultadoUnoDos = momentoInvariante(picture, tamano, centroide, p, q);

			p = 2;
			q = 1;

			momentoResultadoDosUno = momentoInvariante(picture, tamano, centroide, p, q);

			p = 0;
			q = 3;

			double momentoResultadoCeroTres = momentoInvariante(picture, tamano, centroide, p, q);

			double InvarianteTres = invarianteTres(momentoResultadoTresCero, momentoResultadoUnoDos, momentoResultadoDosUno, momentoResultadoCeroTres);

			double InvarianteCuatro = invarianteCuatro(momentoResultadoTresCero, momentoResultadoUnoDos, momentoResultadoCeroTres);

			double InvarianteCinco = invarianteCinco (momentoResultadoTresCero, momentoResultadoUnoDos, momentoResultadoDosUno, momentoResultadoCeroTres);

			Console.WriteLine ("Invariante 1: " + InvarianteUno); 
			Console.WriteLine ("Invariante 2: " + InvarianteDos); 
			Console.WriteLine ("Invariante 3: " + InvarianteTres);
			Console.WriteLine ("Invariante 4: " + InvarianteCuatro);
			Console.WriteLine ("Invariante 5: " + InvarianteCinco);
		}

		public static void Main (string[] args)
		{
			MainClass main = new MainClass ();

			Console.WriteLine ("Imagen Original");
			Bitmap imagenOriginal = main.obtenerImagen();
			main.principal (imagenOriginal);

			Console.WriteLine ("Imagen Rotacion 15");
			Bitmap rotacion = main.rotateImage (15);
			main.principal (rotacion);

			Console.WriteLine ("Imagen Rotacion 35");
			rotacion = main.rotateImage (35);
			main.principal (rotacion);

			Console.WriteLine ("Imagen Rotacion 45");
			rotacion = main.rotateImage (45);
			main.principal (rotacion);

			Console.WriteLine ("Imagen Rotacion 65");
			rotacion = main.rotateImage (65);
			main.principal (rotacion);

			Console.WriteLine ("Imagen Rotacion 75");
			rotacion = main.rotateImage (75);
			main.principal (rotacion);

			Console.WriteLine ("Imagen Rotacion 90");
			rotacion = main.rotateImage (90);
			main.principal (rotacion);

			Console.WriteLine ("Imagen Escala Mitad");
			Bitmap escalaImagen = main.scaleImage(0.5f);
			main.principal (escalaImagen);

			Console.WriteLine ("Imagen Escala Doble");
			escalaImagen = main.scaleImage(1f);
			main.principal (escalaImagen);

		}

	}
}
